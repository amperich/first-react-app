import React, {Component} from 'react'
import Comment from './Comment'

class CommentList extends Component {
    state = {
        isOpen: false
    };

    render() {
        const text = this.state.isOpen ? 'Скрыть комментарии': 'Показать комметарии';
        return (
            <div>
                <button onClick={this.toggleOpen}>{text}</button>
                {this.getBody()}
            </div>
        )
    }

    getBody() {
        if (!this.state.isOpen) return null;

        const {comments} = this.props;
        if (!comments || !comments.length) return <p>Комментариев нет</p>;
        const commentElements = comments.map(comment => <li key={comment.id}><Comment comment = {comment} /></li>);

        return (
            <ul>
                {commentElements}
            </ul>
        )
    }

    toggleOpen = (ev) => {
        ev.preventDefault();
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
}

export default CommentList